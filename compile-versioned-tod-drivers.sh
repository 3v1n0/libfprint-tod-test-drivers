#!/bin/bash
set -ex

versions=(
    "1.90.1"
    "1.90.2"
    "1.90.3"
    "1.90.5"
    # "1.92.0"
    "1.94.0"
    "1.94.3"
    "1.94.5"
    "1.94.6"
    "1.94.7"
)

CURRENT_DIR="$(dirname $0)"
TOD_VERSION="${TOD_VERSION:-1}"
TOD_REPO=${TOD_REPO:-/data/GNOME/libfprint}
OUTPUT_PATH=${OUTPUT_PATH:-$CURRENT_DIR/built-drivers}
COMPILE_VERSION="$1"

host_cpu=$(dpkg-architecture -qDEB_HOST_GNU_CPU)
pkgconfig_path="lib/$(dpkg-architecture -qDEB_HOST_MULTIARCH)/pkgconfig"

if [ ! -d "$TOD_REPO" ] || [ ! -d "$TOD_REPO/.git" ]; then
    TOD_REPO="$(mktemp -d -t -u "tod-build-repo-XXXXXXXX")"
    git clone https://gitlab.freedesktop.org/3v1n0/libfprint.git "$TOD_REPO"
    git config advice.detachedHead false
fi

function compile_version() {
    local version="$1"

    if ! git -C "$TOD_REPO" checkout "v${version}+tod${TOD_VERSION}"; then
        [[ "$version" == "${versions[-1]}" ]] && \
            git -C "$TOD_REPO" checkout "tod" || exit 1
    fi

    local build=$(mktemp -d -t -u "tod-build-$version-XXXXXXXX")
    local install=$(mktemp -d -t "tod-install-$version-XXXXXXXX")
    local versioned_opts=()

    if [[ "$version" != "1.90."* ]]; then
        versioned_opts+="-Dudev_hwdb_dir=$install/lib/udev/hwdb.d"
    fi

    meson "$TOD_REPO" "$build" --prefix="$install" \
        -Dudev_rules_dir="$install/lib/udev/rules.d" \
        -Dintrospection=false \
        -Ddoc=false \
        -Dtod=true \
        "${versioned_opts[@]}"

    ninja install -C "$build"

    build=$(mktemp -d -t -u "tod-driver-build-$version-XXXXXXXX")
    env PKG_CONFIG_PATH="$install/$pkgconfig_path:$PKG_CONFIG_PATH" \
        meson "$CURRENT_DIR" "$build" --buildtype=debug \
            -Dcompatibility_level="$version"
    ninja -C "$build"

    local version_path="$OUTPUT_PATH/tod-$host_cpu-v$TOD_VERSION+$version"
    mkdir -p "$version_path"
    patchelf --shrink-rpath "$build/"*.so
    cp "$build/"*.so "$version_path"
}

if [ -n "$COMPILE_VERSION" ]; then
    versions=($COMPILE_VERSION)
fi

for version in "${versions[@]}"; do
    compile_version "$version"
done
