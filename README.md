## Test libpfprint drivers using libfprint-TOD (libfprint for Touch OEM Drivers)

These drivers are supposed to be compiled using [libfprint-tod](https://gitlab.freedesktop.org/3v1n0/libfprint/tree/tod) at the preferred version that can be explicitly controlled via the
`compatibility_level` option.

### Compile

Once that libfprint light-fork is installed in your prefix, just use

    meson _build [ -Dcompatibility_level=1.94 ]
    ninja -C _build
    # Optional, will install this in the default libfprint-tod drivers path
    ninja -C _build install


### Use and Develop

These drivers are meant to be used as part of the [libfprint-tod](https://gitlab.freedesktop.org/3v1n0/libfprint/tree/tod) test suite.
