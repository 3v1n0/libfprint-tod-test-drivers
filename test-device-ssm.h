/*
 * Virtual driver for device debugging
 *
 * Copyright (C) 2021 Marco Trevisan <marco.trevisan@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#pragma once

#include <tod-1/drivers_api.h>

#define FPI_TYPE_DEVICE_SSM_TEST (fpi_device_fake_get_type ())
G_DECLARE_FINAL_TYPE (FpiDeviceSSMTest, fpi_device_ssm_test, FPI, DEVICE_SSM_TEST, FpDevice)

GType fpi_tod_shared_driver_get_type (void);

struct _FpiDeviceSSMTest
{
  FpDevice parent;
};
